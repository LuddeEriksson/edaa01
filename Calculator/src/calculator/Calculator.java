package calculator;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Calculator extends JFrame implements ActionListener {
	JPanel[] panels = new JPanel[5];
	JButton[] buttons = new JButton[18];
	
	String[] buttonString = { "7", "8", "9", "+",
			
			"4", "5", "6", "-",
			
			"1", "2", "3", "*",
			
			"CSTK", "CLX", "/",
			
			"CHS", "ENTER", "0" };
	int[] dimW = { 300, 45, 100, 90 };
	int[] dimH = { 35, 40 };
	Dimension displayDimension = new Dimension(dimW[0], dimH[0]);
	Dimension regularDimension = new Dimension(dimW[1], dimH[1]);
	Dimension rColumnDimension = new Dimension(dimW[2], dimH[1]);
	Dimension zeroButDimension = new Dimension(dimW[3], dimH[1]);
	boolean[] function = new boolean[4];
	double[] temporary = {0, 0};
	JTextArea display = new JTextArea(1,20);
	Font font = new Font("Comic Sans", Font.BOLD, 14);
	
	public Calculator() {
		super("Calculator");
	//	setDesign();
		setSize(380, 250);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		GridLayout grid = new GridLayout(5,5);
		setLayout(grid);
		for(int i = 0; i < 4; i++){
		    function[i] = false;
		}
		FlowLayout f1 = new FlowLayout(FlowLayout.CENTER);
		FlowLayout f2 = new FlowLayout(FlowLayout.CENTER,1,1);
		for(int i = 0; i < 5; i++){
		    panels[i] = new JPanel();
		}
		for(int i = 0; i < 19; i++) {
		    buttons[i] = new JButton();
		    buttons[i].setText(buttonString[i]);
		    buttons[i].setFont(font);
		    buttons[i].addActionListener(this);
		}
		display.setFont(font);
		display.setEditable(false);
		display.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);




		
		
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
	public static void main(String[] args){
		
	}

}
