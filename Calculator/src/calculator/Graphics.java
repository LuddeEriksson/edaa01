package calculator;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Graphics {
	
	public Graphics(){
		
	}
	
	public void draw(){
		JFrame frame = new JFrame("Calculator");
		frame.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel digits = new JPanel();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		frame.add(digits, c);
		
		JButton b0 = new JButton("0");
		c.weightx= 0.5;
		
		
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		digits.add(b0, c);
		digits.add(b1, c);
		digits.add(b2, c);
		digits.add(b3, c);
		digits.add(b4, c);
		digits.add(b5, c);
		digits.add(b6, c);
		digits.add(b7, c);
		digits.add(b8, c);
		digits.add(b9, c);

		
		
		
		
		frame.pack();
		frame.setVisible(true);
		


	}

}
