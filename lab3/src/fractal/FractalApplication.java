package fractal;

import mountain.Mountain;
import mountain.Point;
import koch.Koch;

public class FractalApplication {
	public static void main(String[] args) {
		Fractal[] fractals = new Fractal[2];
		fractals[0] = new Koch(300);
		Point a = new Point(100, 300);
		Point b = new Point(200, 100);
		Point c = new Point(400, 350);
		fractals[1] = new Mountain(30.0, a, b, c);
	    new FractalView(fractals, "Fraktaler");

	}

}
