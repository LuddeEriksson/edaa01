package mountain;



public class Side {
	private Point a;
	private Point b;
	private Point m;

	public Side(Point a, Point b, Point m) {
		this.a = a;
		this.b = b;
		this.m = m;
	}
//	public Side(Point a, Point b){
//		this.a = a;
//		this.b = b;
//	}

	public Point getMiddle() {

		return m;
	}
	

	public boolean equals(Side x) {
		if (x.getA() == a && x.getB() == b || x.getA() == b && x.getB() == a)  {
			return true;
		}

		return false;
	}

	public Point getA() {
		return a;
	}

	public Point getB() {
		return b;
	}
	

}
