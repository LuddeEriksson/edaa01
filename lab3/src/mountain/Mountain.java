package mountain;

import java.util.Iterator;
import java.util.LinkedList;

import fractal.Fractal;
import fractal.TurtleGraphics;

public class Mountain extends Fractal {
	private Point a;
	private Point b;
	private Point c;
	private double dev;
	private LinkedList<Side> sides;

	public Mountain(double dev, Point a, Point b, Point c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.dev = dev;
		sides = new LinkedList<Side>();

	}

	@Override
	public String getTitle() {
		return "Mountain";
	}

	@Override
	public void draw(TurtleGraphics g) {
		fractalLine(g, order, dev, a, b, c);

	}

	private void fractalLine(TurtleGraphics g, int order, double dev, Point a,
			Point b, Point c) {
		if (order == 0) {
			g.moveTo(a.getX(), a.getY());
			g.forwardTo(b.getX(), b.getY());
			g.forwardTo(c.getX(), c.getY());
			g.forwardTo(a.getX(), a.getY());
		} else {

			Point lilA = calculateM(a, b, dev);
			Point lilB = calculateM(b, c, dev);
			Point lilC = calculateM(a, c, dev);

			dev /= 2;
			fractalLine(g, order - 1, dev, a, lilA, lilC);
			fractalLine(g, order - 1, dev, lilA, lilB, b);
			fractalLine(g, order - 1, dev, c, lilC, lilB);
			fractalLine(g, order - 1, dev, lilA, lilB, lilC);

		}
	}

	public Point calculateM(Point a, Point b, double dev) {
		Iterator<Side> itr = sides.iterator();
		while (itr.hasNext()) {
			Side temp = itr.next();
			if (temp.equals(new Side(a, b, null))) {
				sides.remove(temp);
				return temp.getMiddle();
			}
		}
		Point tempMiddle = new Point((a.getX() + b.getX()) / 2,
				(a.getY() + b.getY()) / 2 + RandomUtilities.randFunc(dev));
		Side tempy = new Side(a, b, tempMiddle);
		sides.add(tempy);
		return tempy.getMiddle();
	}
}
