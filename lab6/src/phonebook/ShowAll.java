package phonebook;
import javax.swing.*;

import java.awt.event.*;
import java.util.List;
import java.util.Set;

@SuppressWarnings("serial")
public class ShowAll extends JMenuItem implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;
	
	public ShowAll(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Show all");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}
	
	 public void actionPerformed(ActionEvent e) {
		 Set<String> set = phoneBook.names();
		 if(!set.isEmpty()){
			 StringBuilder res = new StringBuilder();
			 for(String s: set){
				 res.append(s + "'s numbers are: ");
				 List<String> numero = phoneBook.findNumber(s);
				 for(String c : numero){
					 res.append(c + ", ");
				 }
				 res.append("\n");
				 
			 }
			 gui.setText(res.toString());
		 }
		 else{
			 gui.setText("The phonebook is empty");
		 }
		
	 }
}
