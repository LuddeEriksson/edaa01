package phonebook;

import javax.swing.*;

import java.util.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class FindNumber extends JMenuItem implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;

	public FindNumber(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Find number(s)");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog("Enter name");
		
		
		if(name.isEmpty()){
			gui.getMsgArea().setText(
					"Error: No name was entered");
			return;
		}
		
		List<String> res = phoneBook.findNumber(name);
		if (name != null ) {
			if (!res.isEmpty()) {
				StringBuilder rb = new StringBuilder();
				for (String s : res) {
					rb.append(s + "\n");
					
				}
				gui.setText(rb.toString());
			} else {
				gui.setText(
						"Error: The name " + name + " does not exist");
			}
		}
	}
}
