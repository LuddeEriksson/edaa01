package phonebook;

import javax.swing.*;

import java.util.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class FindName extends JMenuItem implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;

	public FindName(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Find name(s)");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		String number = JOptionPane.showInputDialog("Enter number");
		
		
		if(number.isEmpty()){
			gui.getMsgArea().setText(
					"Error: No number was entered");
		}
		else if (number != null) {
			List<String> res = phoneBook.findNames(number);
			if (!res.isEmpty()) {
				StringBuilder rb = new StringBuilder();
				for (String s : res) {
					rb.append(s + "\n");
					
				}
				gui.setText(rb.toString());
			} else {
				gui.setText(
						"Error: The number " + number + " does not exist");
			}
		}
	}
}
