package phonebook;

import javax.swing.*;

import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;

import java.awt.event.*;

@SuppressWarnings("serial")
public class AddMenu extends JMenuItem implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;

	public AddMenu(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Add");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog("Enter name");
		if(name == null || name.equals("")){
			gui.setText("No name was entered");
			return;
		}
		
		String number = JOptionPane.showInputDialog("Enter number");
		if (number == null || number.isEmpty()) {
			gui.setText("No number entered");
			return;
		}
		
		boolean x = phoneBook.put(name, number);
		if (x) {
			gui.setText(name + " " + number + " added");
		} else {
			gui.setText("Operation failed");

		}

	}
}
