package phonebook;

import javax.swing.*;

import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;

import java.awt.event.*;

@SuppressWarnings("serial")
public class LoadMenu extends JMenuItem implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;

	public LoadMenu(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Load phonebook");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog("Enter filename");
		if (name != null && !name.isEmpty()) {

			phoneBook.load(name);
		}

	}
}
