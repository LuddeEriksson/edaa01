package phonebook;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class QuitButton extends JButton implements ActionListener {
	private PhoneBook phoneBook;
	private PhoneBookGUI gui;

	public QuitButton(PhoneBook phoneBook, PhoneBookGUI gui) {
		super("Quit");
		this.phoneBook = phoneBook;
		this.gui = gui;
		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		int x = JOptionPane.showConfirmDialog(null,
				"Would you like to save your phonebook?",
				"Save", JOptionPane.YES_NO_OPTION);
		if(x == 0){
			String name = JOptionPane.showInputDialog("Enter filename");
			if(name != null && !name.isEmpty()){
				phoneBook.save(name);
			}
			
		}
		System.exit(ABORT);
	}
}
