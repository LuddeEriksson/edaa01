package phonebook;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class PhoneBookGUI extends JFrame {
	private PhoneBook phoneBook;
	private JTextArea messageArea;
	
	public JTextArea getMsgArea(){
		return messageArea;
	}
		
	public PhoneBookGUI(PhoneBook pb) {
		super("PhoneBook");
		phoneBook = pb;
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		Locale.setDefault(new Locale("en"));
		/* To avoid hardcoded Swedish text on OptionPane dialogs */
		UIManager.put("OptionPane.cancelButtonText","Cancel");
		
		setLayout(new BorderLayout());
		JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);
		JMenu editMenu = new JMenu("Edit");
		menubar.add(editMenu);
		editMenu.add(new AddMenu(phoneBook,this));
		editMenu.add(new RemoveMenu(phoneBook,this));
		editMenu.add(new LoadMenu(pb, this));
		
		JMenu find = new JMenu("Find");
		menubar.add(find);
		find.add(new FindName(phoneBook,this));
		find.add(new FindNumber(phoneBook, this));
		JMenu view = new JMenu("View");
		menubar.add(view);
		view.add(new ShowAll(phoneBook, this));
		
			
		
		JPanel southPanel = new JPanel();
		messageArea = new JTextArea(4,25);
		messageArea.setEditable(false);
		southPanel.add(new JScrollPane(messageArea));
		southPanel.add(new QuitButton(phoneBook,this));
		add(southPanel,BorderLayout.CENTER);
		messageArea.setCaretColor(Color.CYAN);
		
		pack();
		setVisible(true);
	}
	
	public void setText(String message){
		messageArea.setText(message);
	}
}
