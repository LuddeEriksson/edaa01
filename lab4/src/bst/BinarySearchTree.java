package bst;

import java.util.Random;

import bst.BinarySearchTree.BinaryNode;

public class BinarySearchTree<E extends Comparable<? super E>> {
	BinaryNode<E> root;
	int size;

	/**
	 * Constructs an empty binary searchtree.
	 */
	public BinarySearchTree() {

	}

	/**
	 * Inserts the specified element in the tree if no duplicate exists.
	 * 
	 * @param x
	 *            element to be inserted
	 * @return true if the the element was inserted
	 */

	public static void main(String[] args) {
		BSTVisualizer bst = new BSTVisualizer("Tree", 400, 400);
		BSTVisualizer after = new BSTVisualizer("After", 400, 400);
		
		BinarySearchTree<Integer> tree = new BinarySearchTree<>();

		for (int i = 0; i < 24; i++) {
			tree.add(i + 1);

		}
		
		bst.drawTree(tree);
		tree.rebuild();
		after.drawTree(tree);
		tree.printTree();
		System.out.println(" Height : " + tree.height() + " Size: " + tree.size());

		// tree.toArray(tree.root, test, 0);

	}

	public boolean add(E x) {
		return add(x, root);
	}

	private boolean add(E x, BinaryNode<E> root) {
		if (size == 0) {
			this.root = new BinaryNode<E>(x);
			size++;
			return true;
		} else {

			if (root.left == null && root.element.compareTo(x) > 0) {
				size++;
				root.left = new BinaryNode<E>(x);
				return true;
			} else if (root.right == null && root.element.compareTo(x) < 0) {

				root.right = new BinaryNode<E>(x);
				size++;
				return true;
			}

			else if (root.element.compareTo(x) > 0) {
				return add(x, root.left);

			} else if (root.element.compareTo(x) < 0) {
				return add(x, root.right);
			} else
				return false;
		}

		// return true;
	}

	/**
	 * Computes the height of tree.
	 * 
	 * @return the height of the tree
	 */
	public int height() {
		return height(root);
	}

	private int height(BinaryNode<E> root) {

		if (root == null)
			return 0;

		else
			return 1 + Math.max(height(root.left), height(root.right));

	}

	/**
	 * Returns the number of elements in this tree.
	 * 
	 * @return the number of elements in this tree
	 */
	public int size() {
		return size;
	}

	/**
	 * Print tree contents in inorder.
	 */
	public void printTree() {
		printTree(root);

	}

	private void printTree(BinaryNode<E> root) {
		
		if (root != null) {
			printTree(root.left);
			System.out.print(root.element + " ");
			printTree(root.right);
		}
	}

	/**
	 * Builds a complete tree from the elements in the tree.
	 */
	public void rebuild() {
		@SuppressWarnings("unchecked")
		E[] a = (E[]) new Comparable[size];
		toArray(root, a, 0);
		root = buildTree(a, 0, a.length - 1);
	}

	/*
	 * Adds all elements from the tree rooted at n in inorder to the array a
	 * starting at a[index]. Returns the index of the last inserted element + 1
	 * (the first empty position in a).
	 */
	private int toArray(BinaryNode<E> root, E[] a, int index) {

		if (root.left != null) {
			toArray(root.left, a, index);
			index++;
		}

		a[index] = root.element;

		if (root.right != null) {
			index++;
			toArray(root.right, a, index);
		}
		return a.length;
	}

	/*
	 * Builds a complete tree from the elements a[first]..a[last]. Elements in
	 * the array a are assumed to be in ascending order. Returns the root of
	 * tree.
	 */
	private BinaryNode<E> buildTree(E[] a, int first, int last) {
		int mid = Math.round(((float) first + (float) last) / 2);
		if (mid > last) {
			return null;
		}
		BinaryNode<E> root = new BinaryNode<E>(a[mid]);
		
		if(first != last){
			
		root.left = buildTree(a, first, mid - 1);
		root.right = buildTree(a, mid + 1, last);
		}
		return root;
	}




	static class BinaryNode<E> {
		E element;
		BinaryNode<E> left;
		BinaryNode<E> right;

		private BinaryNode(E element) {
			this.element = element;
		}
	}

}
