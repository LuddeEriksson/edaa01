package map;

import java.util.ArrayList;

public class SimpleHashMap<K, V> implements Map<K, V> {
	private Entry<K, V>[] entries;
	private int size = 0;
	private double load = 0;

	/**
	 * Constructs an empty hashmap with the default initial capacity (16) and
	 * the default load factor (0.75).
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashMap() {
		this(16);
	}

	/**
	 * Constructs an empty hashmap with the specified initial capacity and the
	 * default load factor (0.75).
	 */
	public SimpleHashMap(int capacity) {
		entries = (Entry<K, V>[]) new Entry[capacity];
	}

	@Override
	public V get(Object arg0) {
		int index = index((K) arg0);
		Entry<K,V> x = find(index, (K) arg0);
		if (x != null) {
			return x.getValue();

		}
		return null;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public V put(K key, V value) {
		int index = index(key);
		Entry<K, V> found = find(index, key);
		if (found == null) {
			Entry<K, V> e = new Entry<K, V>(key, value);
			e.next = entries[index];
			entries[index] = e;
			size++;
			
		} else {
			return found.setValue(value);

		}

		load = (double) size / entries.length;
		if (load >= 0.75) {
			reHash();

		}

		return null;
	}

	private void reHash() {
		Entry<K, V>[] temp = entries;
		entries = new Entry[2 * entries.length];
		size = 0;
		for (Entry<K, V> E : temp) {
			while (E != null) {
				put(E.getKey(), E.getValue());
				E = E.next;
			}
		}
		load = (double) size / entries.length;

	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(Object key) {
		Entry<K, V> found = find(index((K) key), (K) key);
		Entry<K, V> in = entries[index((K) key)];

		if (in == null) {
			return null;

		} else if (in.getKey().equals(key)) {
			Entry<K, V> temp = entries[index((K) key)];
			entries[index((K) key)] = temp.next;
			size--;
			return temp.getValue(); // kolla sen
			
		} else {
			while (in.next != null) {
				if (in.next.getKey().equals(key)) {
					Entry<K, V> temp = in.next;
					in.next = in.next.next;
					size--;
					return temp.getValue();
				}
				in = in.next;
				
			}
		}

		return null;
	}

	@Override
	public int size() {
		return size;
	}

	public String show() {
		String space = " ";
		StringBuilder res = new StringBuilder();
		int i = 0;
		for (Entry E : entries) {
			res.append(i + ". ");
			if (E != null) {
				
				res.append("(" + E.toString() + ")");
				res.append(space);
				Entry<K, V> nextE = E.next;
				while (nextE != null) {
					res.append("(" + nextE + ")");
					res.append(space);
					nextE = nextE.next;
				}
			}
			res.append("\n");
			i++;
		}
		return res.toString();
		// return null;
	}

	private int index(K key) {
		int index = key.hashCode() % entries.length;
		if (index < 0) {
			index += entries.length;
		}
		return index;
	}

	public void printLoad() {
		System.out.println("Load is: " + load);
	}

	private Entry<K, V> find(int index, K key) {

		Entry<K, V> x = entries[index];
		while (x != null) {
			if (x.getKey().equals(key)) {
				return x;
			}
			x = x.next;
		}
		return null;
	}

	public static class Entry<K, V> implements Map.Entry<K, V> {

		private K k;
		private V v;
		private Entry<K, V> next;

		public Entry(K k, V v) {
			this.v = v;
			this.k = k;
		}

		@Override
		public K getKey() {
			return k;
		}

		@Override
		public V getValue() {
			return v;
		}

		@Override
		public V setValue(V value) {
			V temp = v;
			v = value;
			return temp;
		}

		public String toString() {
			return k + " = " + v;
		}

	}

	public static void main(String[] args) {
		SimpleHashMap<Integer, Integer> shm = new SimpleHashMap<Integer, Integer>(
				10);
		for(int i = 0; i < 6; i++){
			shm.put(i, i);
			
		}
		shm.put(10, 10);
		System.out.print(shm.show() + "\nEfter rehash:\n ");
		shm.put(11, 3);
		System.out.print(shm.show());


		shm.printLoad();
	}
}
