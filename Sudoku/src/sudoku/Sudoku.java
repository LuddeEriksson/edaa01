package sudoku;

public class Sudoku {

	private int[][] board;

	public Sudoku() {
		board = new int[9][9];

	}

	/**
	 * Checks if board is ok.
	 * 
	 * @return a bool if the board is OK.
	 */
	public boolean isBoardOK() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != 0) {
					if (!canPlace(i, j, board[i][j])) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Solves puzzle. Returns bool.
	 * 
	 * @return a boolean if solvable/not solvable
	 */
	public boolean solve() {
		return solve(0, 0);
	}

	/**
	 * Tries to solve the puzzle. Returns true if solved.
	 * 
	 * @return true if solved else false.
	 */
	private boolean solve(int x, int y) {
		if (x >= 9) {
			x = 0;
			y++;
			if (y >= 9) {
				return true;
			}
		}

		if (board[x][y] != 0) {
			return solve(x + 1, y);
		} else {
			for (int i = 1; i < 10; i++) {
				if (canPlace(x, y, i)) {
					board[x][y] = i;
					if (solve(x + 1, y)) {
						return true;
					}
				}
			}
			board[x][y] = 0;
		}
		return false;
	}

	/**
	 * @return the values of spot x, y
	 */
	public int getValue(int x, int y) {
		return board[x][y];
	}

	/**
	 * Sets the value of spot x, y
	 * 
	 */
	public void setValue(int x, int y, int value) {
		board[x][y] = value;
	}

	/**
	 * Sets the board
	 * 
	 */
	public void setBoard(int[][] userboard) {
		board = userboard;

	}

	/**
	 * Returns the board
	 * 
	 * @return a board matrix with ints
	 */
	public int[][] getBoard() {
		return board;
	}

	/**
	 * Returns bool if you can place value on x, y
	 * 
	 * @return a bool
	 */
	public boolean canPlace(int x, int y, int value) {
		if (loneHorizontal(x, y, value) && loneField(x, y, value)
				&& loneVertical(x, y, value)) {
			return true;
		}
		return false;
	}

	/**
	 * checks horizontally if any other spot has the same value
	 * 
	 * @return a bool
	 */
	private boolean loneHorizontal(int x, int y, int value) {
		for (int i = 0; i < 9; i++) {
			if(x == i){
				
			}
			else if (getValue(i, y) == value) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if there is no other spot with the smae value vertically
	 * 
	 * @return a bool
	 */
	private boolean loneVertical(int x, int y, int value) {
		for (int i = 0; i < 9; i++) {
			if( i == y){
				
			}
			else if (getValue(x, i) == value) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the value is alone in it's 3x3 field
	 * 
	 * @return a bool
	 */
	private boolean loneField(int x, int y, int value) {
		int xCorner = x - x % 3;
		int yCorner = y - y % 3;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if(xCorner + i == x && yCorner + j == y){
					
				}
				else if (getValue(xCorner + i, yCorner + j) == value) {
					return false;
				}
			}
		}
		return true;

	}

}
