package sudoku;

import javax.swing.*;
import java.awt.event.*;

public class SolveButton extends JButton implements ActionListener {
	private BoardGUI gui;
	private Sudoku s;

	/**
	 * Creates a SolveButton
	 * 
	 * 
	 */
	public SolveButton(BoardGUI gui, Sudoku s) {
		super("Solve");
		this.gui = gui;
		this.s = s;

		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		int[][] userboard = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				
				userboard[i][j] = gui.getNr(i, j);
			}
		}
		s.setBoard(userboard);
		if (s.isBoardOK() && s.solve()) {
			userboard = s.getBoard();
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					gui.setNr(i, j, userboard[i][j]);
					
				}
			}
		} else {
			JOptionPane.showMessageDialog(null,
					"The sudoku is unsolvable", "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}
}