package sudoku;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ClearButton extends JButton implements ActionListener {
	private BoardGUI gui;

	public  ClearButton(BoardGUI gui) {
		super("Clear");
		this.gui= gui;

		addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		gui.clearField();
	}
}