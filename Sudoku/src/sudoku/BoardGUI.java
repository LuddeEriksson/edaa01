package sudoku;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class BoardGUI {
	private JTextField[][] blocks;
	private Sudoku sudoku;

	/**
	 * Clears the board
	 * 
	 */
	public void clearField() {

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {

				blocks[i][j].setText("");

			}
		}
	}

	/**
	 * Returns the number
	 * 
	 * @return the number that cord x,y has
	 */
	public int getNr(int i, int j) {
		int x = 0;
		if (!blocks[i][j].getText().isEmpty()) {
			x = Integer.parseInt(blocks[i][j].getText());
		}

		return x;
	}

	/**
	 * sets the cordinat x,y to value
	 * 
	 */
	public void setNr(int x, int y, int value) {
		Integer a = value;
		blocks[x][y].setText(a.toString());
	}

	/**
	 * Creates the graphical interface
	 * 
	 * 
	 */
	public BoardGUI(Sudoku sudoku) {
		this.sudoku = sudoku;
		JFrame frame = new JFrame();
		frame.setTitle("Sudoku");
		frame.setSize(600, 600);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		Font font1 = new Font("SansSerif", Font.BOLD, 40);

		JPanel back = new JPanel(new GridLayout(9, 9));
		frame.add(back);

		blocks = new JTextField[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				blocks[i][j] = new OneLetterField();
				back.add(blocks[i][j]);
				blocks[i][j].setBorder(border);
				blocks[i][j].setFont(font1);
				blocks[i][j].setHorizontalAlignment(JTextField.CENTER);

			}
		}

		Color d = new Color(0xF280A1);
		Color c = new Color(0x824B77);
		color(0, 3, 0, 3, d);
		color(0, 3, 6, 9, d);
		color(6, 9, 0, 3, d);
		color(6, 9, 6, 9, d);
		color(3, 6, 3, 6, d);
		color(3, 6, 0, 3, c);
		color(0, 3, 3, 6, c);
		color(3, 6, 0, 3, c);
		color(3, 6, 6, 9, c);
		color(6, 9, 3, 6, c);
		
		

		JPanel menu = new JPanel();
		menu.add(new SolveButton(this, sudoku));
		menu.add(new ClearButton(this));
		frame.add(menu, BorderLayout.SOUTH);

		frame.setVisible(true);
	}

	/**
	 * colors the squares entered
	 * 
	 *
	 */
	public void color(int istart, int ifin, int jstart, int jfin, Color color) {
		for (int i = istart; i < ifin; i++) {
			for (int j = jstart; j < jfin; j++) {
				Random r = new Random();

				blocks[i][j].setBackground(color);

			}
		}
	}

}
