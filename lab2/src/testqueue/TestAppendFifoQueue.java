package testqueue;

import static org.junit.Assert.*;

import java.util.Iterator;

import queue.FifoQueue;

import org.junit.Test;

public class TestAppendFifoQueue {
	private FifoQueue<Integer> myIntQueue;
	private FifoQueue<Integer> myIntQueue2;

	@Test
	public void testTwoEmptyQueues() {
		myIntQueue = new FifoQueue<Integer>();
		myIntQueue2 = new FifoQueue<Integer>();
		myIntQueue.append(myIntQueue2);
		Iterator itr = myIntQueue.iterator();
		assertFalse(itr.hasNext());
	}

	@Test
	public void testSecondEmptyQueue() {
		myIntQueue = new FifoQueue<Integer>();
		myIntQueue2 = new FifoQueue<Integer>();
		for (int i = 1; i < 5; i++) {
			myIntQueue.offer(2 * i);
		}
		myIntQueue.append(myIntQueue2);
		assertEquals(4, myIntQueue.size());
		while (myIntQueue.size() != 0) {
			myIntQueue.poll();
		}
	}

	@Test
	public void testFirstEmptyQueue() {
		myIntQueue = new FifoQueue<Integer>();
		myIntQueue2 = new FifoQueue<Integer>();
		for (int i = 1; i < 5; i++) {
			myIntQueue2.offer(2 * i);
		}
		myIntQueue.append(myIntQueue2);
		assertEquals(4, myIntQueue.size());
	}

	@Test
	public void testTwoFilledQueues() {
		myIntQueue = new FifoQueue<Integer>();
		myIntQueue2 = new FifoQueue<Integer>();

		for (int i = 1; i < 5; i++) {
			myIntQueue2.offer(2 * i);
		}

		for (int i = 1; i < 6; i++) {
			myIntQueue.offer(2 * i);
		}
		myIntQueue.append(myIntQueue2);
		assertEquals(9, myIntQueue.size());
		Iterator itr = myIntQueue2.iterator();
		assertFalse(itr.hasNext());

	}

}
