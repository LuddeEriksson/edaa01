package queue;

import java.util.*;

public class FifoQueue<E> extends AbstractQueue<E> implements Queue<E> {
	private QueueNode<E> last;
	private int size;

	public FifoQueue() {
		last = null;
	}

	/**
	 * Returns an iterator over the elements in this queue
	 * 
	 * @return an iterator over the elements in this queue
	 */
	public Iterator<E> iterator() {
		return new QueueIterator();
	}

	/**
	 * Returns the number of elements in this queue
	 * 
	 * @return the number of elements in this queue
	 */
	public int size() {

		return size;
	}

	/**
	 * Inserts the specified element into this queue, if possible post: The
	 * specified element is added to the rear of this queue
	 * 
	 * @param x
	 *            the element to insert
	 * @return true if it was possible to add the element to this queue, else
	 *         false
	 */
	public boolean offer(E x) {
		if (last == null) {
			last = new QueueNode<E>(x);
			last.next = last;
		} else {
			QueueNode<E> nextVar = new QueueNode<E>(x);
			nextVar.next = last.next;
			last.next = nextVar;
			last = nextVar;

		}
		size++;
		return true;
	}

	/**
	 * Retrieves and removes the head of this queue, or null if this queue is
	 * empty. post: the head of the queue is removed if it was not empty
	 * 
	 * @return the head of this queue, or null if the queue is empty
	 */
	public E poll() {
		if (last == null) {
			return null;
		}

		QueueNode<E> head = last.next;
		last.next = last.next.next;
		size--;

		if (size == 0) {
			last = null;
		}

		return head.element;
	}

	/**
	 * Retrieves, but does not remove, the head of this queue, returning null if
	 * this queue is empty
	 * 
	 * @return the head element of this queue, or null if this queue is empty
	 */
	public E peek() {
		if (last == null) {
			return null;
		}
		return last.next.element;
	}

	private static class QueueNode<E> {
		E element; // refererar till elementet
		QueueNode<E> next; // refererar till efterföljande nod

		/* Konstruktor */
		private QueueNode(E x) {
			element = x;
			next = null;
		}

	}

	/**
	 * Appends the specified queue to this queue post: all elements from the
	 * specified queue are appended to this queue. The specified queue (q) is
	 * empty
	 * 
	 * @param q
	 *            the queue to append
	 */
	public void append(FifoQueue<E> q) {
		if (q.last != null) {
			if (size != 0) {
				QueueNode<E> first = last.next;
				last.next = q.last.next;
				q.last.next = first;
			}
			last = q.last;
			size += q.size();
			q.size = 0;
			q.last = null;
		}

	}

	private class QueueIterator implements Iterator<E> {
		private QueueNode<E> pos = null;
		private int counter = 1;

		private QueueIterator() {
			if (last != null) {
				pos = last.next;
			}

		}

		public boolean hasNext() {
			if (pos == null || counter > size) {
				return false;
			}
			return true;
		}

		public E next() {
			if (hasNext()) {
				pos = pos.next;
				counter++;
				return pos.element;
			}
			throw new NoSuchElementException();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

	}
}
