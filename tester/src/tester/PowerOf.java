package tester;

public class PowerOf {
	
	public static double powerOf(double x, int n){
		if(n == 0){
			return 1;
		}
		else{
			return x * powerOf(x, n-1);
		}
	}
	public static void main(String[] args){
		System.out.println(powerOf(5, 200));
	}

}
