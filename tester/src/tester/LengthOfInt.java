package tester;

public class LengthOfInt {
	public static void main(String[] args){
		System.out.println(length(-999809253));
	}
	public static int length(int x){
		 x = Math.abs(x);
		if (x < 10){
			return 1;
		}
		else{
			int a = x / 10;
			return length(a) + 1;
		}
	}

}
