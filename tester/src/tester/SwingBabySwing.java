package tester;

import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingBabySwing {
	private static int clicks;
	private static JFrame frame;
	private static JLabel label;
	public static void main(String[] args){
		clicks = 0;
		drawWindow();

		

	}
	
	public static void drawWindow(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panelTop = new JPanel();
		JPanel panelBott = new JPanel();
		JPanel panelMiddle = new JPanel();
		frame.add(panelMiddle, BorderLayout.CENTER);
		frame.add(panelTop, BorderLayout.NORTH);
		frame.add(panelBott, BorderLayout.SOUTH);
		 label = new JLabel("Number of clicks: " + clicks);
		JButton clicky = new JButton("Click");
		clicky.addActionListener(new Action());
		panelTop.add(clicky);
		panelBott.add(new JButton(" Do Stuff"));
		panelMiddle.add(label);
		frame.pack();
		frame.setVisible(true);
	}
	private static class Action implements ActionListener{
	

		@Override
		public void actionPerformed(ActionEvent e) {
			clicks++;
			label.setText("Number of clicks:" + clicks);
			
			
		}
		
	}

}
