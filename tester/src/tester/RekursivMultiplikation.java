package tester;

public class RekursivMultiplikation {

	public static void main(String[] args) {
		int[] a = {1, 2, 3, 4, 7, 8 , 9, 10};
		System.out.println(sumExist(a, 5));
	}
	public static boolean sumExist(int[] a, int x){
		return sumExist(a, x, 0, a.length-1);
	}
	
	private static boolean sumExist(int[] a, int x, int first, int last){
		if (first == last){
			return false;
		}
		else{
			
			for(int i = first; i < last-1; i++){
				if((a[i] + a[last]) == x){
					return true;
				}
				
			}
		return sumExist(a, x, first, last-1);
		}
	}

}
