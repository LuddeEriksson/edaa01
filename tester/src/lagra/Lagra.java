package lagra;

public class Lagra {
	private int size;
	private Node first;
	private Node last;

	public Lagra() {
		first = new Node(Integer.MIN_VALUE, Integer.MIN_VALUE);
		last = new Node(Integer.MAX_VALUE, Integer.MAX_VALUE);
		first.next = last;
	}
	
	public static void main(String[] args){
		Lagra lagra = new Lagra();
		for(int i = 1; i < 5; i++){
			lagra.insert(i);
		}
		for(int i = 18; i < 24; i++){
			lagra.insert(i);
		}
		lagra.insert(25);
		for(int i = 28; i < 58; i++){
		lagra.insert(i);
		}
		lagra.insert(20);
		lagra.insert(24);
		lagra.insert(17);
		lagra.insert(5);
		lagra.insert(10);
		
		Node temp = lagra.first;
		while(temp != null){
			System.out.println("Min: " + temp.min + " " + "Max: " + temp.max);
			temp = temp.next;
		}
		System.out.println(lagra.size());
	}
	
	

	public int size() {
		return size;
	}

	public boolean contains(int nbr) {
		Node temp = first;
		while (temp != null) {
			if (nbr >= temp.min && nbr <= temp.max) {
				return true;

			}
			temp = temp.next;
		}
		return false;
	}

	public boolean insert(int nbr) {
		if (contains(nbr)) {
			return false;
		} else if (contains(nbr + 1) && !contains(nbr - 1)) {
			Node temp = first;
			while (temp.next != null){
				if(temp.next.min -1 == nbr){
					Node a = temp.next.next;
					temp.next = new Node(nbr, temp.next.max);
					temp.next.next = a;
					size++;
					return true;
				}
				temp = temp.next;
			}
		}
		else if(contains(nbr -1) && !contains(nbr + 1)){
			Node temp = first;
			while(temp.next != null){
				if(temp.next.max == nbr - 1){
					Node a= temp.next.next;
					temp.next = new Node(temp.next.min, nbr);
					temp.next.next = a;
					size++;
					return true;
				}
				temp = temp.next;
			}
		}
		else if(contains(nbr + 1) && contains(nbr - 1)){
			Node temp = first;
			Node one = null;
			Node two = null;
			int min = 0;
			int max = 0;
			while(temp.next != null){
				if(temp.next.min == nbr + 1){
					max = temp.next.max;
					two = temp.next.next;
				}
				else if(temp.next.max == nbr - 1){
					min = temp.next.min;
					one = temp;
					
				}
				temp = temp.next;
			}
			one.next = new Node(min, max);
			one.next.next = two;
			size++;
			return true;
		}
		Node temp = first;
		while(temp != null){
			if(temp.next.min > nbr){
				Node b = new Node(nbr, nbr);
				b.next = temp.next;
				temp.next = b;
				size++;
			//	return true;
			}
			temp = temp.next;
		}
		
		return true;

	}

	private static class Node {
		private int min;
		private int max;
		private Node next;

		private Node(int min, int max) {
			this.min = min;
			this.max = max;
			next = null;
		}
	}
}
