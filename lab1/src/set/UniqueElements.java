package set;

public class UniqueElements {
	private static MaxSet<Integer> maxi;
	
	
	public static void main(String[] args){
		int[] test = new int[]{0, 0, -1, -2, -28, -66, 2,2,5,6,3,5,3,9,1,9};
		int[] correct = uniqueElements(test);
		for(int i: correct){
			System.out.print(i + " ");
		}
			
		
	}

	public UniqueElements() {

	}

	public static int[] uniqueElements(int[] ints) {
		maxi = new MaxSet<Integer>();
		for (int i : ints) {
				maxi.add(i);
		}
		
		int[] result = new int[maxi.size()];
		int minus = 1;
		int maximumimus = maxi.size();
		while(!maxi.isEmpty() ){
			result[maximumimus - minus ] = maxi.getMax();
			maxi.remove(maxi.getMax());
			minus++;
		}

		return result;
	}
}

